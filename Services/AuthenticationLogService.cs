﻿using System;
using System.Linq;
using System.Collections.Generic;
using AuthenticationExample.Models;
using Microsoft.EntityFrameworkCore;
using AuthenticationExample.Helpers;
using AuthenticationExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;

namespace AuthenticationExample.Services
{
    public class AuthenticationLogService : IAuthenticationLogService
    {
        private readonly Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthenticationLogService(Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public void LogMessage(User user, EventNameType eventNameType, string message = null)
        {
            var path = _httpContextAccessor.HttpContext.Request.Path.ToString();
            var ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var userAgent = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"]; 

            if (user == null)
            {
                user = new User();
            }

            _context.AuthenticationLogs.Add(new AuthenticationLog {
                UserId = user.UserId,
                EventName = eventNameType.ToString(),
                Message = message,
                Path = path,
                IpAddress = ipAddress,
                UserAgent = userAgent,
                CreatedById = user.UserId,
                CreatedDate = DateTime.Now
            });            

            _context.SaveChanges();            
        }
    }
}
