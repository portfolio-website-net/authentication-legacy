﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AuthenticationExample.Models;
using AuthenticationExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using AuthenticationExample.Helpers;
using AuthenticationExample.Controllers.Base;

namespace AuthenticationExample.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "Admin,Read")]
    public class SecureController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserProfileService _userProfileService;

        public SecureController(ILogger<HomeController> logger,
            IAuthenticationService authenticationService,
            IUserProfileService userProfileService)
        {
            _logger = logger;
            _authenticationService = authenticationService;
            _userProfileService = userProfileService;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [ClaimRequirement(SecureClaimTypes.Permission, "Admin")]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult NoAccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View(new UserProfileUpdateResult());
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Profile([FromForm] ProfileFields profileFields)
        {
            var result = new UserProfileUpdateResult
            {
                Reason = UserProfileUpdateResultReason.Error
            };
            var user = (User)HttpContext.Items["User"];
            if (user != null)
            {
                profileFields.UserId = user.UserId;
                result = _userProfileService.UpdateUserProfile(profileFields);
            }

            return View(result);
        }
    }
}
