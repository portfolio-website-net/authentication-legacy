﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AuthenticationExample.Models;
using AuthenticationExample.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using AuthenticationExample.Controllers.Base;
using AuthenticationExample.Helpers;
using Microsoft.Extensions.Options;

namespace AuthenticationExample.Controllers
{
    [ClaimRequirement(SecureClaimTypes.Permission, "")]
    public class HomeController : BaseController
    {        
        private readonly ILogger<HomeController> _logger;
        private readonly IAuthenticationService _authenticationService;
        private readonly Settings _settings;

        public HomeController(ILogger<HomeController> logger,
            IAuthenticationService authenticationService,
            IOptions<Settings> settings)
        {
            _logger = logger;
            _authenticationService = authenticationService;
            _settings = settings.Value;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            _authenticationService.PopulateUsersIfEmptyDatabase();

            return View();
        }

        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult SignIn(string username, string password) 
        {
            var authenticationResult = new AuthenticationResult();
            var success = _authenticationService.Authenticate(username, password);
            if (success)
            {
                var sessionId = _authenticationService.CreateSession(username);
                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = true
                };

                Response.Cookies.Append(_settings.SessionCookieName, sessionId, options);

                return RedirectToAction("Index", "Secure");
            }
            else
            {
                authenticationResult.Username = username;
                authenticationResult.AuthenticationResultType = AuthenticationResultType.InvalidCredentials;
                return View("Index", authenticationResult);
            }
        }

        [HttpPost]
        public IActionResult SignOut()
        {
            var sessionCookie = Request.Cookies[_settings.SessionCookieName];
            if (sessionCookie != null)
            {
                _authenticationService.EndSession(sessionCookie);

                var options = new CookieOptions
                {
                    IsEssential = true,
                    HttpOnly = true,
                    Secure = true,
                    Expires = DateTime.Now.AddDays(-1)
                };

                Response.Cookies.Append(_settings.SessionCookieName, string.Empty, options);
                HttpContext.Items["User"] = null;
            }

            var authenticationResult = new AuthenticationResult
            {
                AuthenticationResultType = AuthenticationResultType.SignedOut
            };
            return View("Index", authenticationResult);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
