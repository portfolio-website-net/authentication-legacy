﻿namespace AuthenticationExample
{
    public class Settings
    {
        public string SessionCookieName { get; set; }

        public int SessionTime { get; set; }
    }
}
