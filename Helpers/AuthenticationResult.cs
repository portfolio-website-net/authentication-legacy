namespace AuthenticationExample.Helpers
{
    public class AuthenticationResult
    {
        public string Username { get; set; }

        public AuthenticationResultType AuthenticationResultType { get; set; }
    }

    public enum AuthenticationResultType
    {
        None,
        InvalidCredentials,
        SignedOut
    }
}