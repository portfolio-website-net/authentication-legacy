using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using AuthenticationExample.Services.Interfaces;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Options;

namespace AuthenticationExample.Helpers
{
    // Reference: https://github.com/derekgreer/authorize-example
    public class ClaimRequirementFilter : IAuthorizationFilter
    {
        private readonly Claim _claim;
        private readonly IAuthenticationService _authenticationService;
        private readonly Settings _settings;
        private readonly IAuthenticationLogService _authenticationLogService;

        public ClaimRequirementFilter(
            Claim claim, 
            IAuthenticationService authenticationService,
            IOptions<Settings> settings,
            IAuthenticationLogService authenticationLogService)
        {
            _claim = claim;
            _authenticationService = authenticationService;
            _settings = settings.Value;
            _authenticationLogService = authenticationLogService;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var path = context.HttpContext.Request.Path.ToString();
            var sessionCookie = context.HttpContext.Request.Cookies[_settings.SessionCookieName];

            var currentUser = _authenticationService.GetCurrentUser(sessionCookie);
            if (currentUser != null)
            {
                var claims = new List<string>();
                if (!string.IsNullOrEmpty(_claim.Value))
                {
                    claims = _claim.Value.Split(',').Select(x => x.Trim()).ToList();
                }

                var hasClaim = _claim.Value == string.Empty
                    || currentUser.ClaimsList.Where(x => claims.Contains(x)).Any();
                if (!hasClaim)
                {
                    _authenticationLogService.LogMessage(currentUser, EventNameType.NoAccessToSecurePage);
                    context.Result = new RedirectResult("/Secure/NoAccess");
                }
                else
                {
                    _authenticationLogService.LogMessage(currentUser, EventNameType.PageView);
                    context.HttpContext.Items["User"] = currentUser;
                }

                _authenticationService.RenewSession(sessionCookie);
            }
            else if (path.ToLower().StartsWith("/Secure".ToLower()))
            {
                _authenticationLogService.LogMessage(currentUser, EventNameType.AccessDenied);
                context.Result = new RedirectResult("/");
            }
            else
            {
                _authenticationLogService.LogMessage(null, EventNameType.PageView);
            }
        }
    }
}