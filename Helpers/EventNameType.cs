namespace AuthenticationExample.Helpers
{
    public enum EventNameType
    {
        FailedSignIn,
        SignIn,
        SignOut,
        PageView,
        ProfileUpdate,
        UsernameUpdate,
        PasswordUpdate,
        PermissionUpdate,
        InvalidProfilePassword,
        UsernameMustBeUnique,
        PasswordsDoNotMatch,
        ProfileUpdateError,
        NoAccessToSecurePage,
        AccessDenied
    }
}