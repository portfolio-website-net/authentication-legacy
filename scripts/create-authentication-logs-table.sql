IF OBJECT_ID('dbo.[AuthenticationLogs]', 'U') IS NOT NULL 
BEGIN 
	DROP TABLE dbo.[AuthenticationLogs] 
END

CREATE TABLE [dbo].[AuthenticationLogs](
	[AuthenticationLogId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[EventName] [nvarchar](100) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Path] [nvarchar](max) NULL,
	[IpAddress] [nvarchar](50) NULL,	
	[UserAgent] [nvarchar](max) NULL,
	[CreatedById] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedById] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	CONSTRAINT [PK_AuthenticationLogs] PRIMARY KEY CLUSTERED 
	(
		[AuthenticationLogId] ASC
	)
) 
GO
