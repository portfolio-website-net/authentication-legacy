﻿var sessionTimeout = null
var countDownInterval = null
var secondsRemaining = 60
var sessionTime = 10

function initializeSessionTimeout() {
    secondsRemaining = 60

    if (sessionTimeout) {
        clearTimeout(sessionTimeout)
    }

    if (countDownInterval) {
        clearInterval(countDownInterval)
    }

    sessionTimeout = setTimeout(function() {
        $('#sessionTimeoutModal').modal({
            keyboard: false,
            backdrop: 'static'
        })

        $('#remainingTimeLabel').html(secondsRemaining + ' seconds')

        countDownInterval = setInterval(function() {
            $('#remainingTimeLabel').html(secondsRemaining + ' seconds')
            secondsRemaining--

            if (secondsRemaining == 0) {
                clearInterval(countDownInterval)
                $('#SignOutForm').submit()
            }
        }, 1000)

    }, (sessionTime - 1) * 60 * 1000)
}            

function renewSession() {
    $.ajax(
        {
            url: baseUrl + 'Home/Index', 
            type: 'GET',
            success: function(result) {
                initializeSessionTimeout()
            },
            error: function (jqXHR, textStatus, errorThrown) {                            
            }
        }
    )
}